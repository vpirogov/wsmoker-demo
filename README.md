## TODO

- [ ] разобраться с путями внутри wsmoker
- [ ] оформить библиотекой
- [ ] реализовать протокол управления в controlHandler и control.js
- [ ] поразмыслить над messageType
- [ ] переименовать controlHandler.getList()

## Команды управления:

- [ ] `list` запрашивает список подключений к моку
  - идентификаторы в формате `${host}:${port}`
- [ ] `send` запрашивает отправку ws-сообщения по идентификатору подключения
- [ ] `subscribe` запросить подписку на сообщений mock-сессии
- [ ] `unsubscribe` отказаться от подписки

## Инициализация

```go
import (
	"log"
	"wsmoker-test/wsmoker"

	"github.com/gorilla/websocket"
)

func main() {
	port := 8080
	wsmoker.Serve(port, handler)
}

func handler(conn *websocket.Conn) {
	for {
		messageType, p, err := conn.ReadMessage()
        // бизнес-логика мока
	}
}
```

Если передан nil, то запускается встроенный эхо-handler:

```go
import (
	"wsmoker-test/wsmoker"
)

func main() {
	port := 8080
	wsmoker.Serve(port, nil)
}
```

## End-points

- `HTTP /` - web-страница управления
- `WS /ws/mock` - mocked websocket
- `HTTP /mock` - стартует эмуляцию мока (однако...)
