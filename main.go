package main

import (
	"os"
	"wsmoker-test/wsmoker"
)

func main() {
	port := 8080
	wsmoker.Serve(port, nil, os.Stdout)
}
