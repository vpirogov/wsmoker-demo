package wsmoker

import (
	"github.com/gorilla/websocket"
)

type registry struct {
	hash map[string]*websocket.Conn
	list []string
}

// NewRegistry creates an implementation of
// Registrator and Hijacker interfaces
func NewRegistry() *registry {
	return &registry{
		hash: make(map[string]*websocket.Conn),
		list: make([]string, 0, 5),
	}
}

func (r *registry) List() []string {
	return r.list
}

func (r *registry) Send(name string, messageType int, msg []byte) error {
	if conn, found := r.hash[name]; found {
		return conn.WriteMessage(messageType, msg)
	}
	return NotFoundError
}

func (r *registry) Add(name string, conn *websocket.Conn) {
	r.hash[name] = conn
	r.list = append(r.list, name)
}

func (r *registry) Remove(name string) {
	delete(r.hash, name)
	idx := func() int {
		for i, val := range r.list {
			if val == name {
				return i
			}
		}
		return 0
	}()
	r.list = append(r.list[:idx], r.list[idx+1:]...)
}
