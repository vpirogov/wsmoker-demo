package wsmoker

import (
	"log"
	"net/http"
	"text/template"
)

type httpHandler struct {
	templateFile string
}

func (h httpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles(h.templateFile)
	if err != nil {
		log.Fatal(err)
	}
	tmpl.Execute(w, nil)
}
