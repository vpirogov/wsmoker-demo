package wsmoker

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

// TODO inject logger
type controlHandler struct {
	Hijacker
	log *log.Logger
}

func newControlHandler(hj Hijacker, l *log.Logger) *controlHandler {
	return &controlHandler{Hijacker: hj, log: l}
}

func (h *controlHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		h.log.Println(err)
		return
	}

	h.handle(conn)
}

func (h *controlHandler) handle(conn *websocket.Conn) {
	for {
		messageType, p, err := conn.ReadMessage()
		if err != nil {
			h.log.Println(err)
			return
		}
		h.log.Printf("Got WS type #%v control message: '%s'", messageType, string(p))

		var m IncomingMessage
		err = json.Unmarshal(p, &m)
		if err != nil {
			h.log.Println(err)
			continue
		}
		h.log.Println(m)
		switch m.Command {
		case "list":
			p, err = json.Marshal(ListMessage{h.List()})
			if err != nil {
				h.log.Println(err)
				continue
			}
			h.log.Println(string(p))
			if err := conn.WriteMessage(messageType, p); err != nil {
				h.log.Println(err)
				return
			}
		}

	}
}
