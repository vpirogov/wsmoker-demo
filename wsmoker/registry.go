package wsmoker

import (
	"errors"

	"github.com/gorilla/websocket"
)

var (
	NotFoundError = errors.New("Item not found")
)

type Registrator interface {
	Add(key string, conn *websocket.Conn)
	Remove(key string)
}

type Hijacker interface {
	List() []string
	Send(key string, msgType int, msg []byte) error
}
