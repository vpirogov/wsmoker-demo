const makeLogger = (selector, className) => {
    const target = document.querySelector(selector)

    return (text, alt) => {
        const ts = (new Date()).toISOString().slice(0,19).replace('T',' ')
        const li = document.createElement('LI')
        li.innerHTML = `${ts} ${text}`
        if (alt) li.classList.add(className)
        target.prepend(li)
    }
}