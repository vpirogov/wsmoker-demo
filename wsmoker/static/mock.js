const secured = document.location.protocol === 'https' ? 's' : ''
const URL = `ws${secured}://${document.location.host}/ws/mock`
const messageSelector = 'input#message'
const sendButtonSelector = 'input#send'
const disconnectButtonSelector = 'input#disconnect'
const logSelector = 'ul#messagelog'
const logAltClassName = 'highlight'

const log = makeLogger(logSelector, logAltClassName)

document.addEventListener('DOMContentLoaded', () => {
    console.log('Mock JS loaded')

    const ws = connect(URL, messageHandler)

    document.querySelector(sendButtonSelector).addEventListener('click', () => {
        const el = document.querySelector(messageSelector)
        log(`Send message '${el.value}'`)
        ws.send(el.value)
        el.value = ''
    })

    document.querySelector(disconnectButtonSelector).addEventListener('click', () => {
        for (const el of document.querySelectorAll(".controls")) {
            el.disabled = true
        }
        log(`Disconnecting`, true)
        ws.send(`Bye-bye`)
        ws.close()
    })
})


const messageHandler = m => {
    log(`Got WS message: '${m.data}'`, true)
}

const connect = (URL, handler) => {
    const openHandler = ({target: ws}) => {
        console.info(`WS is connected to '${URL}'`)
        ws.send(`Hello from JS`)
    }
    
    const closeHandler = e => {
        if (e.wasClean) {
            console.info(`WS is clean closed by ${e.reason}`)
        } else {
            console.warn(`WS is closed ${e.code}`)
        }
    }
    
    const ws = new WebSocket(URL)

    ws.onopen = openHandler
    ws.onclose = closeHandler
    ws.onerror = e => console.error(e)
    ws.onmessage = handler

    return ws
}