const secured = document.location.protocol === 'https' ? 's' : ''
const URL = `ws${secured}://${document.location.host}/ws/control`
const logSelector = 'ul#messagelog'
const logAltClassName = 'highlight'

const log = makeLogger(logSelector, logAltClassName)

document.addEventListener('DOMContentLoaded', () => {
    console.log('Control JS loaded', document.location.host, )

    const ws = connect(URL, messageHandler)

    setInterval(() => {
        ws.send(JSON.stringify({"command":"list"}))
    }, 1000)
})

const messageHandler = m => {
    log(`Got WS message: '${m.data}'`, true)
    // console.log(`Got WS message: '${m.data}'`, true)
    let data
    try {
        data = JSON.parse(m.data)
    } catch (e) {
        console.warn(`Can't parse incoming message '${m.data}'`)
    }
    // console.dir({event: 'Got message', data})
}

const connect = (URL, handler) => {
    const openHandler = ({target: ws}) => {
        console.info(`WS is connected to '${URL}'`)
        // ws.send(JSON.stringify({"command":"list"}))
    }
    
    const closeHandler = e => {
        if (e.wasClean) {
            console.info(`WS is clean closed by ${e.reason}`)
        } else {
            console.warn(`WS is closed ${e.code}`)
        }
    }
    
    const ws = new WebSocket(URL)

    ws.onopen = openHandler
    ws.onclose = closeHandler
    ws.onerror = e => console.error(e)
    ws.onmessage = handler

    return ws
}

