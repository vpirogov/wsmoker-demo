package wsmoker

import (
	"log"

	"github.com/gorilla/websocket"
)

func NewEchoHandler(logger *log.Logger) func(conn *websocket.Conn) {
	return func(conn *websocket.Conn) {
		for {
			messageType, p, err := conn.ReadMessage()
			if err != nil {
				logger.Println(err)
				return
			}
			logger.Printf("Got WS type #%v message: '%s'", messageType, string(p))
			if err := conn.WriteMessage(messageType, p); err != nil {
				logger.Println(err)
				return
			}
		}
	}
}
