package wsmoker

import "fmt"

type IncomingMessage struct {
	Command string
	Target  string `json:"target,omitempty"`
	Data    string `json:"data,omitempty"`
}

func (m IncomingMessage) String() string {
	return fmt.Sprintf("Command:%s Target:%s Data:%s", m.Command, m.Target, m.Data)
}

type ListMessage struct {
	List []string `json:"list"`
}
