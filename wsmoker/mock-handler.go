package wsmoker

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

type mockHandler struct {
	Registrator
	handler func(*websocket.Conn)
	log     *log.Logger
}

func newMockHandler(reg Registrator, handler func(*websocket.Conn), lg *log.Logger) *mockHandler {
	return &mockHandler{
		Registrator: reg,
		handler:     handler,
		log:         lg,
	}
}

func (h *mockHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		h.log.Println(err)
		return
	}

	h.Add(r.RemoteAddr, conn)
	defer h.Remove(r.RemoteAddr)

	// TODO inject logger
	h.log.Printf("'%s' connected", r.RemoteAddr)
	defer h.log.Printf("'%s' disconnected", r.RemoteAddr)

	h.handler(conn)
}
