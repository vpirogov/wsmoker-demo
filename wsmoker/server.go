package wsmoker

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"path/filepath"

	"github.com/gorilla/websocket"
)

const (
	loggerFlags = log.Ldate | log.Ltime | log.Lshortfile | log.Lmsgprefix
)

func Serve(port int, mockWSHandler func(*websocket.Conn), logWriter io.Writer) {
	logger := log.New(logWriter, "wsmoker ", loggerFlags)
	socket := fmt.Sprintf(":%d", port)
	logger.Printf("started HTTP server on %s", socket)

	// Static dir handler
	dir, err := filepath.Abs("wsmoker/static")
	if err != nil {
		logger.Fatal(err)
	}
	staticHandler := http.FileServer(http.Dir(dir))
	http.Handle("/static/", http.StripPrefix("/static/", staticHandler))

	// Websocket handlers
	reg := NewRegistry()
	// Mock
	ml := log.New(logWriter, "mock-handler ", loggerFlags)
	wsHandler := mockWSHandler
	if wsHandler == nil {
		wsHandler = NewEchoHandler(ml)
	}
	mh := newMockHandler(reg, wsHandler, ml)
	http.Handle("/ws/mock", mh)
	// Control
	cl := log.New(logWriter, "ctrl-handler ", loggerFlags)
	ch := newControlHandler(reg, cl)
	http.Handle("/ws/control", ch)

	// HTTP pages
	http.Handle("/mock", httpHandler{templateFile: "wsmoker/templates/mock.html"})
	http.Handle("/", httpHandler{templateFile: "wsmoker/templates/control.html"})
	logger.Fatal(http.ListenAndServe(socket, nil))
}
